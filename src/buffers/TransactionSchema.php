<?php
namespace Catapult\Buffers;

class TransactionSchema{
    public function transferTransactionSchema(){
        return &schema{
            []schemaAttribute{
                newScalarAttribute("size", IntSize),
                newArrayAttribute("signature", ByteSize),
                newArrayAttribute("signer", ByteSize),
                newScalarAttribute("version", ShortSize),
                newScalarAttribute("type", ShortSize),
                newArrayAttribute("maxFee", IntSize),
                newArrayAttribute("deadline", IntSize),
                newArrayAttribute("recipient", ByteSize),
                newScalarAttribute("messageSize", ShortSize),
                newScalarAttribute("numMosaics", ByteSize),
                newTableAttribute("message", schema{
                    []schemaAttribute{
                        newScalarAttribute("type", ByteSize),
                        newArrayAttribute("payload", ByteSize),
                    },
                }.schemaDefinition),
                newTableArrayAttribute("mosaics", schema{
                    []schemaAttribute{
                        newArrayAttribute("id", IntSize),
                        newArrayAttribute("amount", IntSize),
                    },
                }.schemaDefinition),
            },
        }
    }
}
?>