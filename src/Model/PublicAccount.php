<?php
namespace NEM\Model;
use NEM\Model\Address;

class PublicAccount{

    private $address;//Address

    private $publicKey;//string

    public function __construct($address = null, $publicKey = null){
        $this->address = $address;
        $this->publicKey = $publicKey;
    }

    public function getAddress(){
        return $this->address;
    }

    public function getPublicKey(){
        return $this->publicKey;
    }
}
?>