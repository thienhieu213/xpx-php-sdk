<?php
/**
 * Part of the evias/nem-php package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under MIT License.
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    evias/nem-php
 * @version    1.0.0
 * @author     Grégory Saive <greg@evias.be>
 * @author     Robin Pedersen (https://github.com/RobertoSnap)
 * @license    MIT License
 * @copyright  (c) 2017-2018, Grégory Saive <greg@evias.be>
 * @link       http://github.com/evias/nem-php
 */
namespace NEM\Model;

use NEM\Model\TransactionType;
use NEM\Model\Transaction\Transfer;
use NEM\Model\Transaction\Signature;
use NEM\Model\Account;
use NEM\Model\Message;

class Transaction{

    const SignatureSize = 64;
    const SizeSize = 4;
    const SignerSize = 32;

    /**
     * object
     * 
     * included fields type, networkType, version, deadline, fee, signature, signer, transactionInfo
     */
    private $abstractTransaction;

    public function createTransactionHash(string $p){
        $hex = new \NEM\Utils\Hex;
        $b = $hex->DecodeString($p);

        $HalfOfSignature = self::SignatureSize / 2;

        $b1 = array_slice($b,self::SizeSize,$HalfOfSignature);
        $b2 = array_slice($b,self::SizeSize + self::SignatureSize, count($b) - self::SizeSize - self::SignatureSize);
        $sb = array_merge($b1,$b2);

        $sha3Hasher = new \NEM\Core\Sha3Hasher;


        $r = $sha3Hasher->hash("sha3-256",implode(array_map("chr", $sb)));

        return $r;
    }

    public function getAbstractTransaction(){
        return $this->abstractTransaction;
    }

    public function setAbstractTransaction($abstractTransaction){
        return $this->abstractTransaction = $abstractTransaction;
    }

    public function ToAggregate($signer){
        $this->abstractTransaction->signer = $signer;
    }
}
