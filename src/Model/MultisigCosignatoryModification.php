<?php
namespace NEM\Model;

class MultisigCosignatoryModification{
    private $type;

    private $publicAccount;

    public function __construct($type, $publicAccount){
        $this->type = $type;
        $this->publicAccount = $publicAccount;
    }

    public function getType(){
        return $this->type;
    }

    public function getPublicAccount(){
        return $this->publicAccount;
    }
}
?>