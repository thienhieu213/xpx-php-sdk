<?php
namespace NEM\Model\Transaction;

class Constants{
     /**
     * The number of bytes in an `byte`.
     */
    const SIZEOF_BYTE = 1;
    /**
     * The number of bytes in a `short`.
     */
    const SIZEOF_SHORT = 2;
    /**
     * The number of bytes in an `int`.
     */
    const SIZEOF_INT = 4;
    /**
     * The number of bytes in an `float`.
     */
    const SIZEOF_FLOAT = 4;
    /**
     * The number of bytes in an `long`.
     */
    const SIZEOF_LONG = 8;
    /**
     * The number of bytes in an `double`.
     */
    const SIZEOF_DOUBLE = 8;
    /**
     * The number of bytes in a file identifier.
     */
    const FILE_IDENTIFIER_LENGTH = 4;
}
?>