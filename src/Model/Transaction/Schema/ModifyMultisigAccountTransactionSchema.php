<?php
namespace NEM\Model\Transaction\Schema;
use NEM\Model\Transaction\Attribute\ScalarAttribute;
use NEM\Model\Transaction\Attribute\ArrayAttribute;
use NEM\Model\Transaction\Attribute\TableArrayAttribute;
use NEM\Model\Transaction\Constants;

class ModifyMultisigAccountTransactionSchema extends Schema{
    public function __construct() {
        $arr = array(
            new ScalarAttribute("size",Constants::SIZEOF_INT),
            new ArrayAttribute("signature", Constants::SIZEOF_BYTE),
            new ArrayAttribute("signer", Constants::SIZEOF_BYTE),
            new ScalarAttribute("version", Constants::SIZEOF_SHORT),
            new ScalarAttribute("type", Constants::SIZEOF_SHORT),
            new ArrayAttribute("fee", Constants::SIZEOF_INT),
            new ArrayAttribute("deadline", Constants::SIZEOF_INT),
            
            new ScalarAttribute("minRemovalDelta", Constants::SIZEOF_BYTE),
            new ScalarAttribute("minApprovalDelta", Constants::SIZEOF_BYTE),
            new ScalarAttribute("numModifications", Constants::SIZEOF_BYTE),
            new TableArrayAttribute("modifications", array(
                    new ScalarAttribute("type", Constants::SIZEOF_BYTE),
                    new ArrayAttribute("cosignatoryPublicKey", Constants::SIZEOF_BYTE)
            ))
        );
        parent::__construct($arr);
    }
}
?>