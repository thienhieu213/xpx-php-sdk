<?php
namespace NEM\Model;

class MultisigCosignatoryModificationType
{    
    /**
     * @internal
     * @var integer
     */
    const ADD = 0;

    /**
     * @internal
     * @var integer
     */
    const REMOVE = 1;

}
