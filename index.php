<?php
    require "vendor/autoload.php";
    use NEM\API as Nis2Api;
    use NEM\Crypto as Crypto;
    use NEM\Model as Model;
///////////////////////////// Account ////////////////////////////////
    // $api = new Nis2Api\AccountRoutesApi();
    // $accountId = "VC6UUGZLEIAGRXCJXMEUEH2QE7VVMYC3Z67RVRIN"; // {String} Account address or publicKey
    
    // //synchronous
    // $data = $api->getAccountInfo($accountId);
    // echo "API called successfully. Returned data: " . $data, PHP_EOL;

    // $acountIds = new \stdClass();;
    // $acountIds->addresses = array("VC6UUGZLEIAGRXCJXMEUEH2QE7VVMYC3Z67RVRIN","VCTSYT3SPBID36GQDZRC3E4XOUQGIGF5CG6EQXRT");
    // $arr = array("VC6UUGZLEIAGRXCJXMEUEH2QE7VVMYC3Z67RVRIN","VCTSYT3SPBID36GQDZRC3E4XOUQGIGF5CG6EQXRT");
    // $data2 = $api->getAccountsInfo($arr);
    // echo "----------------------";
    // echo "API called successfully. Returned data: " ."<br>";
    // print_r($data2);

//////////////////////////////////Transaction///////////////////////////
    // $transaction = new Nis2Api\TransactionRoutesApi();
    // $transactionId = "5D008458259698000144EA4B";
    // $transactionIds = array("5D008458259698000144EA4B","5D008392259698000144EA2F");
    // $hash = "FCE7BF7CAD42BF7E3928B7E21F96EB51412D2D79A2A4E72B620F31E162C2CA21";
    // $hash_arr = array("FCE7BF7CAD42BF7E3928B7E21F96EB51412D2D79A2A4E72B620F31E162C2CA21","A1DA5CB080F61BD1C311C2E7470B7F4EEAF003F3255840C099079294A60F1F1F");
    // $payload = "9B000000AD31BA4A5BD2E77F7FBD214B094C570CC2B203AA49571A5164EB38BD2D45B3CCEBFB1D8D523F019A7F05C1201F0FE0855B55544236BE971247DF5C5D1D8AAD03990585BBB7C97BB61D90410B67552D82D30738994BA7CF2B1041D1E0A6E4169B03A854410000000000000000EACC8D8617000000A8AD04B3F632D8F9F7EEAF743213509F9E9EC855BAAF669F500700000048656C6C6F21";
    // $data = $transaction->announceTransaction($payload);
    // echo "Resovle: ";
    // print_r($data);
    
/////////////////////////////////Block///////////////////////////////////
    // $transaction = new Nis2Api\BlockchainRoutesApi();
    // $data = $transaction->getBlockchainScore();
    // echo "Resovle: ";
    // print_r($data);
////////////////////////////////Mosaic/////////////////////////////////
    //  $mosaic = new Nis2Api\MosaicRoutesApi();
    //  $mosaicId = array("0DC67FBE1CAD29E3");
    //  $data = $mosaic->getMosaicsName($mosaicId);
    //  echo "Resovle: ";
    //  var_dump($data);

    // $a = 481110499;
    // var_dump( sprintf("%08X", $a) );
    // $b = 231112638;
    // var_dump( sprintf("%08X", $b) );
//////////////////////// Namespace ///////////////////////////
    // $namespace = new Nis2Api\NamespaceRoutesApi();
    // $pub = "952C2E8302D2C657BC96A6FC8D72018A55F8B521A3AFC7903C88023D92CEF205";
    // $arr = array("952C2E8302D2C657BC96A6FC8D72018A55F8B521A3AFC7903C88023D92CEF205");
    // $data = $namespace->getNamespacesFromAccount($pub);
    // //echo "Resovle: ";
    // var_dump($data);

    // $sha3 = new Crypto\Sha3Hasher();
    // $ripemd160 = new Crypto\Ripemd160Hasher();
    // $algo = "sha3-256";
    // $data = "Hello";
    // $output = $ripemd160->hash($data);
    // echo $output;
    $networkType = new Model\NetworkType();
    $config = new Model\Config();
    $Address = new Model\Address();
    $keypair = new Crypto\KeyPair();
    //var_dump($keypair->privateKey);
    //echo $networkType->getNetworkType();

    /////////////////////////////Demo 1 ///////////////////////////////////
    $baseURL = "http://bctestnet1.xpxsirius.io:3000";
    $networkType->setNetworkType("PublicTest");
    $publicKey = "803BD90020E0BB5F0B03AC75C86056A4D4AB5940F2A3A520694D8E7FF217E961";
    // $timeReconnect = 10000; //10s 
    //$netType = $networkType->getNetworkType();
    // //$conf = $config->NewConfig($baseURL,$netType,$timeReconnect);
    //$Address->NewAddressFromPublicKey($publicKey,$netType);
    //var_dump($Address->address);
    // var_dump($conf);
    // echo $conf->BaseURLs;
    //$KeyPair = $keypair->NewRandomKeyPair();
    // var_dump($KeyPair->privateKey->getRaw());
    
    // class A{
    //     const B = 0;
    // }
    // $test = new A();
    // echo $test::B;
    $Ed25519Field = new Crypto\Ed25519Field();
    $Ed25519Field->Ed25519FieldD();
?>